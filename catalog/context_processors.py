"""Context processors for catalog."""

from django.conf import settings


def common_context(request):
    """Set context shared by all views."""

    return {"settings": settings}
