from django.template import Context, Template
from django.test import TestCase
from django.urls import reverse

from catalog.templatetags import catalog


class FakeRequest:
    def __init__(self, path="", full_path="") -> None:
        self.path = path
        self._full_path = full_path if full_path else path

    def get_full_path(self):
        return self._full_path


class TestRatingStars(TestCase):
    def test_should_return_correct_numbers_1(self):
        # when
        result = catalog.rating_stars(1)
        # then
        self.assertDictEqual(
            result,
            {
                "score": 1.0,
                "full_stars_range": range(1),
                "has_half_star": False,
                "empty_stars_range": range(4),
            },
        )

    def test_should_return_correct_numbers_2(self):
        # when
        result = catalog.rating_stars(3.6)
        # then
        self.assertDictEqual(
            result,
            {
                "score": 3.6,
                "full_stars_range": range(3),
                "has_half_star": True,
                "empty_stars_range": range(1),
            },
        )

    def test_should_return_correct_numbers_3(self):
        # when
        result = catalog.rating_stars(0)
        # then
        self.assertDictEqual(
            result,
            {
                "score": 0.0,
                "full_stars_range": range(0),
                "has_half_star": False,
                "empty_stars_range": range(5),
            },
        )

    def test_should_return_correct_numbers_4(self):
        # when
        result = catalog.rating_stars(5)
        # then
        self.assertDictEqual(
            result,
            {
                "score": 5.0,
                "full_stars_range": range(5),
                "has_half_star": False,
                "empty_stars_range": range(0),
            },
        )

    def test_should_return_correct_numbers_5(self):
        # when
        result = catalog.rating_stars(3.9)
        # then
        self.assertDictEqual(
            result,
            {
                "score": 3.9,
                "full_stars_range": range(4),
                "has_half_star": False,
                "empty_stars_range": range(1),
            },
        )

    def test_should_handle_too_high_score(self):
        # when
        result = catalog.rating_stars(10)
        # then
        self.assertDictEqual(
            result,
            {
                "score": 5.0,
                "full_stars_range": range(5),
                "has_half_star": False,
                "empty_stars_range": range(0),
            },
        )

    def test_should_handle_too_small_score(self):
        # when
        result = catalog.rating_stars(-5)
        # then
        self.assertDictEqual(
            result,
            {
                "score": 0.0,
                "full_stars_range": range(0),
                "has_half_star": False,
                "empty_stars_range": range(5),
            },
        )

    def test_should_handle_none(self):
        # when
        result = catalog.rating_stars(None)
        # then
        self.assertIsNone(result)

    def test_should_handle_non_numeric_input(self):
        # when
        result = catalog.rating_stars("wrong")
        # then
        self.assertIsNone(result)


class TestNavActive(TestCase):
    def test_should_return_active_when_key_in_path(self):
        # given
        context = {"request": FakeRequest(path="alpha")}
        # when
        result = catalog.nav_active(context, "alpha")
        # then
        self.assertEqual(result, "active")

    def test_should_return_empty_string_when_key_not_in_path(self):
        # given
        context = {"request": FakeRequest(path="bravo")}
        # when
        result = catalog.nav_active(context, "alpha")
        # then
        self.assertEqual(result, "")

    def test_should_return_empty_string_when_request_object_is_missing(self):
        # given
        context = {}
        # when
        result = catalog.nav_active(context, "alpha")
        # then
        self.assertEqual(result, "")

    def test_should_return_empty_string_when_request_object_has_no_path(self):
        # given
        context = {"request": "abc"}
        # when
        result = catalog.nav_active(context, "alpha")
        # then
        self.assertEqual(result, "")

    def test_should_return_active_when_key_in_path_and_key_given_in_parts(self):
        # given
        context = {"request": FakeRequest(path="alpha-is-glorious")}
        # when
        result = catalog.nav_active(context, "alpha", "-is", "-glorious")
        # then
        self.assertEqual(result, "active")


class TestNavActiveUrl(TestCase):
    def test_should_return_active_when_url_in_path(self):
        # given
        template = Template("{% load catalog %}{% nav_active_url 'faq' %}")
        context = Context({"request": FakeRequest(path=reverse("faq"))})
        # when
        result = template.render(context)
        # then
        self.assertEqual(result, "active")

    def test_should_return_nothing_when_url_not_in_path(self):
        # given
        template = Template("{% load catalog %}{% nav_active_url 'index' %}")
        context = Context({"request": FakeRequest(path=reverse("faq"))})
        # when
        result = template.render(context)
        # then
        self.assertEqual(result, "")

    def test_should_return_active_when_url_in_path_with_args(self):
        # given
        template = Template("{% load catalog %}{% nav_active_url 'app-list' 'all' %}")
        context = Context(
            {"request": FakeRequest(path=reverse("app-list", args=["all"]))}
        )
        # when
        result = template.render(context)
        # then
        self.assertEqual(result, "active")

    def test_should_return_active_when_url_in_path_with_kwargs(self):
        # given
        template = Template(
            "{% load catalog %}{% nav_active_url 'app-list' category='all' %}"
        )
        context = Context(
            {"request": FakeRequest(path=reverse("app-list", args=["all"]))}
        )
        # when
        result = template.render(context)
        # then
        self.assertEqual(result, "active")

    def test_should_return_empty_string_when_request_object_is_missing(self):
        # given
        template = Template(
            "{% load catalog %}{% nav_active_url 'app-list' category='all' %}"
        )
        context = Context({})
        # when
        result = template.render(context)
        # then
        self.assertEqual(result, "")


class TestPagingUrl(TestCase):
    def test_should_handle_url_without_query(self):
        # given
        context = {"request": FakeRequest(full_path="/mysite/alpha")}
        # when
        result = catalog.paging_url(context, "3")
        # then
        self.assertEqual(result, "/mysite/alpha?page=3")

    def test_should_handle_url_with_other_query(self):
        # given
        context = {"request": FakeRequest(full_path="/mysite/alpha?search=xxx")}
        # when
        result = catalog.paging_url(context, "3")
        # then
        self.assertEqual(result, "/mysite/alpha?search=xxx&page=3")

    def test_should_handle_url_with_page_query(self):
        # given
        context = {"request": FakeRequest(full_path="/mysite/alpha?page=1")}
        # when
        result = catalog.paging_url(context, "3")
        # then
        self.assertEqual(result, "/mysite/alpha?page=3")

    def test_should_handle_url_with_mixed_query(self):
        # given
        context = {"request": FakeRequest(full_path="/mysite/alpha?search=xxx&page=1")}
        # when
        result = catalog.paging_url(context, "3")
        # then
        self.assertEqual(result, "/mysite/alpha?search=xxx&page=3")

    def test_should_handle_missing_page(self):
        # given
        context = {"request": FakeRequest(full_path="/mysite/alpha")}
        # when
        result = catalog.paging_url(context, None)
        # then
        self.assertEqual(result, "/mysite/alpha")
