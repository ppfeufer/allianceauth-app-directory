from io import StringIO
from unittest.mock import patch

from django.core.management import CommandError, call_command
from django.test import TestCase
from eve_auth.tools.test_tools import create_fake_user

from catalog.core import community_creations
from catalog.models import App

from .testdata.factories import AppFactory, CategoryFactory, PypiProjectInfoFactory


@patch(
    "catalog.management.commands.appdir_run_batch.Command.create_apps_from_community_list",
    lambda *args, **kwargs: None,
)
@patch("catalog.management.commands.appdir_run_batch.App.update_from_pypi")
class TestRunBatch(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        user = create_fake_user(1001, "Bruce Wayne")
        CategoryFactory(is_default=True)
        AppFactory(maintainers=[user])
        AppFactory(maintainers=[user])

    def test_should_run_update_of_all_apps(self, App_update_from_pypi):
        # given
        App_update_from_pypi.return_value = None
        # when
        out = StringIO()
        call_command("appdir_run_batch", stdout=out)
        # then
        self.assertEqual(App_update_from_pypi.call_count, 2)
        self.assertGreater(len(out.getvalue()), 0)

    def test_should_run_update_of_all_apps_silently(self, App_update_from_pypi):
        # given
        App_update_from_pypi.return_value = None
        # when
        out = StringIO()
        call_command("appdir_run_batch", "--silent", stdout=out)
        # then
        self.assertEqual(App_update_from_pypi.call_count, 2)
        self.assertEqual(out.getvalue(), "")


@patch("catalog.models.pypi.project_info")
class TestImportPypi(TestCase):
    def test_should_import_new_app_with_default_category(self, pypi_project_info):
        # given
        pypi_project_info.return_value = PypiProjectInfoFactory()
        category = CategoryFactory(is_default=True)
        # when
        call_command("appdir_import_pypi", "dummy", stdout=StringIO())
        # then
        app = App.objects.get(pypi_name="dummy")
        self.assertEqual(app.name, "dummy")
        self.assertEqual(app.category, category)
        self.assertTrue(app.is_verified)

    def test_should_import_multiple_apps_with_default_category(self, pypi_project_info):
        # given
        pypi_project_info.return_value = PypiProjectInfoFactory()
        CategoryFactory(is_default=True)
        # when
        call_command("appdir_import_pypi", "dummy-1", "dummy-2", stdout=StringIO())
        # then
        apps_created = App.objects.values_list("pypi_name", flat=True)
        self.assertSetEqual(set(apps_created), {"dummy-1", "dummy-2"})

    def test_should_import_new_app_with_custom_category(self, pypi_project_info):
        # given
        pypi_project_info.return_value = PypiProjectInfoFactory()
        CategoryFactory(is_default=True)
        category = CategoryFactory(name="custom")
        # when
        call_command(
            "appdir_import_pypi", "dummy", "--category", "custom", stdout=StringIO()
        )
        # then
        app = App.objects.get(pypi_name="dummy")
        self.assertEqual(app.name, "dummy")
        self.assertEqual(app.category, category)
        self.assertTrue(app.is_verified)

    def test_should_abort_when_no_category_defined(self, _):
        # when/then
        with self.assertRaises(CommandError):
            call_command("appdir_import_pypi", "dummy", stdout=StringIO())

    def test_should_skip_apps_that_already_exist(self, pypi_project_info):
        # given
        pypi_project_info.return_value = PypiProjectInfoFactory()
        CategoryFactory(is_default=True)
        AppFactory(pypi_name="dummy", description="do-not-update")
        # when
        call_command("appdir_import_pypi", "dummy", stdout=StringIO())
        # then
        app = App.objects.get(pypi_name="dummy")
        self.assertEqual(app.description, "do-not-update")

    def test_should_skip_apps_that_pypi_does_not_know(self, pypi_project_info):
        # given
        pypi_project_info.return_value = None
        CategoryFactory(is_default=True)
        # when
        call_command("appdir_import_pypi", "dummy", stdout=StringIO())
        # then
        self.assertEqual(App.objects.count(), 0)


@patch("catalog.management.commands.appdir_run_batch.App.update_from_pypi")
@patch("catalog.management.commands.appdir_run_batch.Command.update_apps_from_pypi")
@patch("catalog.management.commands.appdir_run_batch.pypi")
@patch("catalog.management.commands.appdir_run_batch.community_creations")
class TestCreateFromCommunityCreationList(TestCase):
    def test_should_create_new_app_from_community_list(
        self,
        mock_community_creations,
        mock_pypi,
        mock_update_apps_from_pypi,
        mock_update_from_pypi,
    ):
        # given
        mock_community_creations.repositories.return_value = [
            community_creations._Repo(
                "gitlab.com", "/ErikKalkoken/aa-freight", "aa-freight"
            )
        ]
        mock_pypi.projects.return_value = {"aa-freight"}
        mock_update_apps_from_pypi.return_value = None
        mock_update_from_pypi.return_value = None
        CategoryFactory(is_default=True)
        # when
        out = StringIO()
        call_command("appdir_run_batch", stdout=out)
        # then
        app_names = set(App.objects.values_list("pypi_name", flat=True))
        self.assertSetEqual(app_names, {"aa-freight"})

    def test_should_create_unknown_app_only_from_community_list(
        self,
        mock_community_creations,
        mock_pypi,
        mock_update_apps_from_pypi,
        mock_update_from_pypi,
    ):
        # given
        mock_community_creations.repositories.return_value = [
            community_creations._Repo(
                "gitlab.com", "/ErikKalkoken/aa-freight", "aa-freight"
            ),
            community_creations._Repo(
                "gitlab.com", "/ErikKalkoken/aa-moonmining", "aa-moonmining"
            ),
        ]
        mock_pypi.projects.return_value = {"aa-freight", "aa-moonmining"}
        mock_update_apps_from_pypi.return_value = None
        mock_update_from_pypi.return_value = None
        category = CategoryFactory(is_default=True)
        existing_app = AppFactory(
            name="Dummy", pypi_name="aa-moonmining", category=category
        )
        # when
        out = StringIO()
        call_command("appdir_run_batch", stdout=out)
        # then
        app_names = set(App.objects.values_list("pypi_name", flat=True))
        self.assertSetEqual(app_names, {"aa-freight", "aa-moonmining"})
        existing_app.refresh_from_db()
        self.assertEqual(existing_app.name, "Dummy")
