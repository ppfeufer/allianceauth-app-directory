import datetime as dt
from unittest.mock import patch

from pytz import utc

from django.core.exceptions import ValidationError
from django.test import TestCase, override_settings
from eve_auth.tools.test_tools import create_fake_user

from catalog.models import App, Review, ReviewReply

from .testdata.factories import AppFactory, CategoryFactory
from .testdata.load_pypi import gen_pypi_info

MODULE_PATH = "catalog.models"


class TestAppContentType(TestCase):
    def test_should_create_from_pypi_markdown(self):
        # when
        result = App.ContentType.from_pypi_type("text/markdown")
        # then
        self.assertEqual(result, App.ContentType.MARKDOWN)

    def test_should_create_from_pypi_text(self):
        # when
        result = App.ContentType.from_pypi_type("text/plain")
        # then
        self.assertEqual(result, App.ContentType.PLAIN)

    def test_should_create_from_pypi_other(self):
        # when
        result = App.ContentType.from_pypi_type("unknown-type")
        # then
        self.assertEqual(result, App.ContentType.UNKNOWN)


class TestApp(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.category = CategoryFactory()
        cls.user = create_fake_user(1001, "Bruce Wayne")

    def test_should_return_name(self):
        # given
        app = AppFactory(name="Dummy", category=self.category)
        app.maintainers.add(self.user)
        # when
        result = str(app)
        # then
        self.assertEqual(result, "Dummy")

    @patch(MODULE_PATH + ".pypi.project_info")
    def test_should_raise_error_when_pypi_name_not_found(self, pypi_project_info):
        # given
        pypi_project_info.return_value = None
        app = App(name="Dummy", pypi_name="dummy", category=self.category)
        # when
        with self.assertRaises(ValidationError):
            app.clean()

    @patch(MODULE_PATH + ".pypi.project_info")
    def test_should_update_from_pypi_project_when_new(self, pypi_project_info):
        # given
        my_first_published = dt.datetime(2020, 1, 2, 3, 12, 0, tzinfo=utc)
        my_last_published = dt.datetime(2021, 6, 11, 14, 12, 0, tzinfo=utc)
        pypi_project_info.return_value = gen_pypi_info(
            first_published=my_first_published, last_published=my_last_published
        )
        app = App(name="Dummy", pypi_name="dummy", category=self.category)

        # when
        app.clean()

        # then
        self.assertTrue(pypi_project_info.called)
        self.assertEqual(app.description_content_type, App.ContentType.MARKDOWN)
        self.assertEqual(
            app.homepage_url, "https://gitlab.com/ErikKalkoken/aa-structures"
        )
        self.assertEqual(app.license, "MIT License")
        self.assertEqual(
            app.summary, "App for managing Eve Online structures with Alliance Auth"
        )
        self.assertEqual(app.version, "1.12.0")
        self.assertEqual(app.first_published, my_first_published)
        self.assertEqual(app.last_published, my_last_published)
        self.assertListEqual(
            app.requirements,
            [
                "allianceauth (>=2.9)",
                "django-eveuniverse (>=0.16)",
                "allianceauth-app-utils (>=1.13)",
            ],
        )

    @patch(MODULE_PATH + ".pypi.project_info")
    def test_should_not_update_from_pypi_project_on_edit(self, pypi_project_info):
        # given
        pypi_project_info.return_value = None
        app = AppFactory(name="Dummy", pypi_name="dummy", category=self.category)
        pypi_project_info.mock_reset()
        # when
        app.clean()
        # then
        self.assertFalse(pypi_project_info.called)

    @patch(MODULE_PATH + ".pypi.project_info")
    def test_should_update_existing_app_from_pypi(self, pypi_project_info):
        # given
        my_first_published = dt.datetime(2020, 1, 2, 3, 12, 0, tzinfo=utc)
        my_last_published = dt.datetime(2021, 6, 11, 14, 12, 0, tzinfo=utc)
        pypi_project_info.return_value = gen_pypi_info(
            first_published=my_first_published, last_published=my_last_published
        )
        app = AppFactory(category=self.category)
        # when
        app.update_from_pypi()
        # then
        self.assertTrue(pypi_project_info.called)
        self.assertEqual(app.authors.count(), 1)
        author_obj = app.authors.first()
        self.assertEqual(author_obj.name, "Erik Kalkoken")
        self.assertEqual(author_obj.email, "kalkoken87@gmail.com")
        self.assertEqual(app.description_content_type, App.ContentType.MARKDOWN)
        self.assertEqual(
            app.homepage_url, "https://gitlab.com/ErikKalkoken/aa-structures"
        )
        self.assertEqual(app.license, "MIT License")
        self.assertEqual(
            app.summary, "App for managing Eve Online structures with Alliance Auth"
        )
        self.assertEqual(app.version, "1.12.0")
        self.assertEqual(app.first_published, my_first_published)
        self.assertEqual(app.last_published, my_last_published)
        self.assertListEqual(
            app.requirements,
            [
                "allianceauth (>=2.9)",
                "django-eveuniverse (>=0.16)",
                "allianceauth-app-utils (>=1.13)",
            ],
        )
        self.assertSetEqual(
            set(app.classifiers.values_list("name", flat=True)),
            {
                "Environment :: Web Environment",
                "Framework :: Django",
                "Framework :: Django :: 3.1",
                "Intended Audience :: End Users/Desktop",
                "License :: OSI Approved :: MIT License",
                "Operating System :: OS Independent",
                "Programming Language :: Python",
                "Programming Language :: Python :: 3.6",
                "Programming Language :: Python :: 3.7",
                "Programming Language :: Python :: 3.8",
                "Topic :: Internet :: WWW/HTTP",
                "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
            },
        )

    @override_settings(BLACKLIST=["blocked"])
    @patch(MODULE_PATH + ".pypi.project_info")
    def test_should_raise_error_when_app_is_blacklisted(self, pypi_project_info):
        # given
        my_first_published = dt.datetime(2020, 1, 2, 3, 12, 0, tzinfo=utc)
        my_last_published = dt.datetime(2021, 6, 11, 14, 12, 0, tzinfo=utc)
        pypi_project_info.return_value = gen_pypi_info(
            first_published=my_first_published,
            last_published=my_last_published,
        )
        app = AppFactory(category=self.category, pypi_name="blocked")
        # when
        with self.assertRaises(ValidationError):
            app.clean()

    def test_should_return_description_in_html_from_markdown(self):
        # given
        app = AppFactory(
            name="Dummy",
            pypi_name="dummy",
            category=self.category,
            description="# Structures",
            description_content_type=App.ContentType.MARKDOWN,
        )
        app.maintainers.add(self.user)
        # when
        result = app.description_html
        # then
        self.assertEqual(result, "<h1>Structures</h1>")

    def test_should_return_description_in_plan_from_plain(self):
        # given
        app = AppFactory(
            name="Dummy",
            category=self.category,
            description="# Structures",
            description_content_type=App.ContentType.PLAIN,
        )
        app.maintainers.add(self.user)
        # when
        result = app.description_html
        # then
        self.assertEqual(result, "# Structures")

    def test_should_return_valid_icon_url_1(self):
        # given
        app = AppFactory(name="Dummy", category=self.category, icon="truck.png")
        app.maintainers.add(self.user)
        # when
        result = app.valid_icon_url
        # then
        self.assertEqual(result, "/media/truck.png")

    def test_should_return_valid_icon_url_2(self):
        # given
        app = AppFactory(name="Dummy", category=self.category)
        app.maintainers.add(self.user)
        # when
        result = app.valid_icon_url
        # then
        self.assertEqual(result, "/static/catalog/img/fake_icons/icon_d.png")

    def test_should_return_requirement_list(self):
        # given
        app = AppFactory(
            category=self.category,
            requirements=[
                "allianceauth>=2.15.1",
                "allianceauth-app-utils>=1.12",
                "dhooks-lite>=0.5.0",
                "django-navhelper",
            ],
        )
        # when
        result = app.requirement_list()
        # then
        first = result[0]
        self.assertEqual(first["name"], "allianceauth")
        self.assertEqual(first["full"], "allianceauth>=2.15.1")
        self.assertFalse(first["is_known"])


class TestAppUpdateRequires(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.category = CategoryFactory()

    def test_should_update_used_by_1(self):
        # given
        alpha = AppFactory(name="alpha", category=self.category, requirements=["bravo"])
        bravo = AppFactory(name="bravo", category=self.category)
        # when
        alpha.update_requires()
        # then
        self.assertIn(bravo, alpha.requires.all())
        self.assertIn(alpha, bravo.used_by.all())

    def test_should_update_used_by_2(self):
        # given
        alpha = AppFactory(name="alpha", category=self.category)
        bravo = AppFactory(name="bravo", category=self.category)
        alpha.requires.add(bravo)
        # when
        alpha.update_requires()
        # then
        self.assertNotIn(bravo, alpha.requires.all())
        self.assertNotIn(alpha, bravo.used_by.all())


class TestCategory(TestCase):
    def test_should_generate_and_sanitize_name(self):
        # when
        category = CategoryFactory(verbose_name="Test öäü yo")
        # then
        self.assertEqual(category.name, "test-oau-yo")
        self.assertEqual(category.verbose_name, "Test öäü yo")

    def test_should_return_verbose_name(self):
        # when
        category = CategoryFactory(verbose_name="Test")
        # then
        self.assertEqual(str(category), "Test")


class TestReview(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.category_1 = CategoryFactory(is_default=True)
        cls.user_1001 = create_fake_user(1001, "Bruce Wayne")
        cls.user_1002 = create_fake_user(1002, "Peter Parker")
        cls.app_1 = AppFactory(
            name="App 1",
            pypi_name="app_1",
            category=cls.category_1,
            summary="alpha",
            description="omega",
            is_verified=True,
        )
        cls.app_1.maintainers.add(cls.user_1001)

    def test_str(self):
        # given
        review = Review.objects.create(app=self.app_1, author=self.user_1002, rating=3)
        # when
        result = str(review)
        # then
        self.assertEqual(result, f"App {self.app_1.pk}-{review.pk}")


class TestReviewReply(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.category_1 = CategoryFactory(verbose_name="Category 1", is_default=True)
        cls.user_1001 = create_fake_user(1001, "Bruce Wayne")
        cls.user_1002 = create_fake_user(1002, "Peter Parker")
        cls.app_1 = AppFactory(
            name="App 1",
            pypi_name="app_1",
            category=cls.category_1,
            summary="alpha",
            description="omega",
            is_verified=True,
        )
        cls.app_1.maintainers.add(cls.user_1001)

    def test_str(self):
        # given
        review = Review.objects.create(app=self.app_1, author=self.user_1002, rating=3)
        reply = ReviewReply.objects.create(
            review=review, author=self.user_1001, details="abc"
        )
        # when
        result = str(reply)
        # then
        self.assertEqual(result, str(review))
