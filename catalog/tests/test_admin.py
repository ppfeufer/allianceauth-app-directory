from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from eve_auth.tools.test_tools import create_fake_user

from .testdata.factories import AppFactory, AuthorFactory, CategoryFactory


class TestAdminPages(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.superuser = User.objects.create(
            username="Superman", is_staff=True, is_superuser=True
        )
        cls.maintainer = create_fake_user(1001, "Bruce Wayne")
        cls.category = CategoryFactory()
        cls.author = AuthorFactory()
        cls.app = AppFactory(authors=[cls.author], maintainers=[cls.maintainer])

    def setUp(self) -> None:
        self.client.force_login(self.superuser)
        return super().setUp()

    def test_should_open_app_changelist(self):
        # when
        response = self.client.get(reverse("admin:catalog_app_changelist"))
        # then
        self.assertEqual(response.status_code, 200)

    def test_should_open_app_change(self):
        # when
        response = self.client.get(
            reverse("admin:catalog_app_change", args=[self.app.id])
        )
        # then
        self.assertEqual(response.status_code, 200)

    def test_should_open_author_changelist(self):
        # when
        response = self.client.get(reverse("admin:catalog_author_changelist"))
        # then
        self.assertEqual(response.status_code, 200)

    def test_should_open_author_details_change(self):
        # when
        response = self.client.get(
            reverse("admin:catalog_author_change", args=[self.author.id])
        )
        # then
        self.assertEqual(response.status_code, 200)

    def test_should_open_category_changelist(self):
        # when
        response = self.client.get(reverse("admin:catalog_category_changelist"))
        # then
        self.assertEqual(response.status_code, 200)

    def test_should_open_category_details_change(self):
        # when
        response = self.client.get(
            reverse("admin:catalog_category_change", args=[self.category.id])
        )
        # then
        self.assertEqual(response.status_code, 200)
