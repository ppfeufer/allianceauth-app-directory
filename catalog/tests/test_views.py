from unittest.mock import patch
from urllib.parse import quote

from django.test import TestCase
from django.urls import reverse
from eve_auth.tools.test_tools import create_fake_user

from catalog.models import App, Review
from catalog.views import AddPageTitleMixin, SpecialCategory

from .testdata.factories import AppFactory, AuthorFactory, CategoryFactory
from .testdata.load_pypi import pypi_info


class TemplateViewStub:
    def get_context_data(self, **kwargs):
        return dict()


class MixinDummy1(AddPageTitleMixin, TemplateViewStub):
    page_title = "my-title"


class MixinDummy2(AddPageTitleMixin, TemplateViewStub):
    pass


class TestMixins(TestCase):
    def test_should_add_to_title(self):
        # given
        obj = MixinDummy1()
        # when
        context = obj.get_context_data()
        # then
        self.assertEqual(context["page_title"], "my-title")

    def test_should_add_nothing(self):
        # given
        obj = MixinDummy2()
        # when
        context = obj.get_context_data()
        # then
        self.assertNotIn("page_title", context)


class TestBasicViews(TestCase):
    def test_should_open_index(self):
        # when
        response = self.client.get(reverse("index"))
        # then
        self.assertEqual(response.status_code, 200)

    def test_should_open_faq(self):
        # when
        response = self.client.get(reverse("faq"))
        # then
        self.assertEqual(response.status_code, 200)


class TestAppListView(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.category_1 = CategoryFactory(is_default=True)
        cls.category_2 = CategoryFactory()
        cls.user_bruce = create_fake_user(1001, "Bruce Wayne")
        cls.user_peter = create_fake_user(1002, "Peter Parker")
        cls.author_batman = AuthorFactory(name="Batman")
        cls.author_lex = AuthorFactory(name="Lex Luthor")
        cls.app_1 = AppFactory(
            name="App 1",
            pypi_name="app_1",
            category=cls.category_1,
            authors=[cls.author_batman],
            summary="alpha",
            description="omega",
            is_verified=True,
            maintainers=[cls.user_bruce],
        )
        cls.app_2 = AppFactory(
            name="App 2",
            pypi_name="app_2",
            category=cls.category_2,
            authors=[cls.author_lex],
            summary="bravo",
            description="tau",
            is_verified=True,
            maintainers=[cls.user_peter],
        )
        Review.objects.create(app=cls.app_2, author=cls.user_bruce, rating=3)
        cls.app_3 = AppFactory(
            name="App 3",
            pypi_name="app_3",
            category=cls.category_2,
            authors=[cls.author_batman],
            summary="charlie",
            description="psi",
            is_verified=True,
            maintainers=[cls.user_peter],
        )
        Review.objects.create(app=cls.app_3, author=cls.user_bruce, rating=4)
        cls.app_4 = AppFactory(
            name="App 4",
            pypi_name="app_4",
            category=cls.category_2,
            authors=[cls.author_lex],
            summary="delta",
            description="delta x",
            is_verified=False,
            maintainers=[cls.user_peter],
        )

    @staticmethod
    def apps_in_response(response) -> set:
        return {
            app.name
            for app in App.objects.all()
            if app.name in response.rendered_content
        }

    def test_should_show_all_verified_apps(self):
        # when
        response = self.client.get(reverse("app-list", args=[SpecialCategory.ALL_APPS]))
        # then
        self.assertEqual(response.status_code, 200)
        self.assertSetEqual(
            self.apps_in_response(response), {"App 1", "App 2", "App 3"}
        )

    def test_should_show_users_apps_incl_not_verified(self):
        # given
        self.client.force_login(self.user_peter)
        # when
        response = self.client.get(reverse("app-list", args=[SpecialCategory.MY_APPS]))
        # then
        self.assertEqual(response.status_code, 200)
        self.assertSetEqual(
            self.apps_in_response(response), {"App 2", "App 3", "App 4"}
        )

    def test_should_filter_top_rated(self):
        # when
        response = self.client.get(
            reverse("app-list", args=[SpecialCategory.TOP_RATED])
        )
        # then
        self.assertEqual(response.status_code, 200)
        self.assertSetEqual(self.apps_in_response(response), {"App 2", "App 3"})

    def test_should_filter_unverified(self):
        # given
        self.client.force_login(self.user_peter)
        # when
        response = self.client.get(
            reverse("app-list", args=[SpecialCategory.UNVERIFIED])
        )
        # then
        self.assertEqual(response.status_code, 200)
        self.assertSetEqual(self.apps_in_response(response), {"App 4"})

    def test_should_show_apps_with_category(self):
        # when
        response = self.client.get(reverse("app-list", args=[self.category_1.name]))
        # then
        self.assertEqual(response.status_code, 200)
        self.assertSetEqual(self.apps_in_response(response), {"App 1"})

    def test_should_show_apps_with_search_by_name(self):
        # when
        response = self.client.get(reverse("app-list") + "?search=" + quote("App 1"))
        # then
        self.assertEqual(response.status_code, 200)
        self.assertSetEqual(self.apps_in_response(response), {"App 1"})

    def test_should_show_apps_with_search_by_author(self):
        # when
        response = self.client.get(reverse("app-list") + "?search=" + quote("Batman"))
        # then
        self.assertEqual(response.status_code, 200)
        self.assertSetEqual(self.apps_in_response(response), {"App 1", "App 3"})

    def test_should_show_apps_with_search_by_maintainer(self):
        # when
        response = self.client.get(
            reverse("app-list") + "?search=" + quote("Peter Parker")
        )
        # then
        self.assertEqual(response.status_code, 200)
        self.assertSetEqual(self.apps_in_response(response), {"App 2", "App 3"})

    def test_should_show_apps_with_search_by_summary(self):
        # when
        response = self.client.get(reverse("app-list") + "?search=" + quote("alpha"))
        # then
        self.assertEqual(response.status_code, 200)
        self.assertSetEqual(self.apps_in_response(response), {"App 1"})

    def test_should_show_apps_with_search_by_description(self):
        # when
        response = self.client.get(reverse("app-list") + "?search=" + quote("omega"))
        # then
        self.assertEqual(response.status_code, 200)
        self.assertSetEqual(self.apps_in_response(response), {"App 1"})

    def test_should_redirect_to_all_by_default(self):
        # given
        self.client.force_login(self.user_bruce)
        # when
        response = self.client.get(reverse("app-list"))
        # then
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
            response.url, reverse("app-list", args=[SpecialCategory.ALL_APPS])
        )

    def test_should_show_empty_list_when_called_with_unknown_category(self):
        # when
        response = self.client.get(reverse("app-list", args=["unknown-category"]))
        # then
        self.assertEqual(response.status_code, 200)
        self.assertSetEqual(self.apps_in_response(response), set())

    def test_should_show_apps_from_maintainers(self):
        # when
        response = self.client.get(
            reverse("app-list") + f"?maintainer={self.user_peter.username}"
        )
        # then
        self.assertEqual(response.status_code, 200)
        self.assertSetEqual(self.apps_in_response(response), {"App 2", "App 3"})

    def test_should_show_apps_from_authors(self):
        # when
        response = self.client.get(
            reverse("app-list") + f"?author={self.author_lex.name}"
        )
        # then
        self.assertEqual(response.status_code, 200)
        self.assertSetEqual(self.apps_in_response(response), {"App 2"})


@patch("catalog.core.pypi.project_info", lambda x: pypi_info)
class TestApps(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.category = CategoryFactory(verbose_name="Category", is_default=True)
        cls.user_1001 = create_fake_user(1001, "Bruce Wayne")
        cls.user_1002 = create_fake_user(1002, "Peter Parker")

    def test_should_not_update_app_from_someone_else(self):
        # given
        my_app = AppFactory(
            name="App 1",
            pypi_name="app-1",
            category=self.category,
            summary="alpha",
            description="omega",
        )
        my_app.maintainers.add(self.user_1002)
        self.client.force_login(self.user_1001)
        # when
        res = self.client.post(
            reverse("app-update", args=[my_app.pypi_name]),
            {"name": "App 2", "pypi_name": "app-1", "category": 1},
        )
        # then
        self.assertEqual(res.status_code, 404)
        my_app.refresh_from_db()
        self.assertEqual(my_app.name, "App 1")

    def test_should_not_delete_app_from_someone_else(self):
        # given
        my_app = AppFactory(
            name="App 1",
            pypi_name="app-1",
            category=self.category,
            summary="alpha",
            description="omega",
        )
        my_app.maintainers.add(self.user_1002)
        self.client.force_login(self.user_1001)
        # when
        res = self.client.post(reverse("app-delete", args=[my_app.pypi_name]))
        # then
        self.assertEqual(res.status_code, 404)
        self.assertTrue(App.objects.filter(pypi_name=my_app.pypi_name).exists())


class TestReviews(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        category = CategoryFactory(verbose_name="Category")
        cls.user_1001 = create_fake_user(1001, "Bruce Wayne")
        cls.app_slug = "app-1"
        cls.my_app = AppFactory(
            name="App 1",
            pypi_name=cls.app_slug,
            category=category,
            summary="alpha",
            description="omega",
        )
        cls.my_app.maintainers.add(cls.user_1001)
        cls.user_1002 = create_fake_user(1002, "Peter Parker")

    def test_should_not_delete_review_from_someone_else(self):
        # given
        review = Review.objects.create(
            app=self.my_app, author=self.user_1002, summary="summary", rating=3
        )
        self.client.force_login(self.user_1001)
        # when
        res = self.client.post(reverse("review-delete", args=[review.pk]))
        # then
        self.assertEqual(res.status_code, 404)
        self.assertTrue(Review.objects.filter(pk=review.pk).exists())


class TestExportDataView(TestCase):
    def test_can_run_export_data_view(self):
        # given
        category = CategoryFactory(verbose_name="Category", is_default=True)
        AppFactory(
            name="App 1",
            pypi_name="app-1",
            category=category,
            summary="alpha",
            description="omega",
        )
        user = create_fake_user(1001, "Bruce Wayne")
        user.is_staff = True
        user.save()
        self.client.force_login(user)
        # when
        res = self.client.get(reverse("export-data"))
        # then
        self.assertEqual(res.status_code, 200)
        self.assertEqual(res.headers["Content-Type"], "text/csv")
