from pathlib import Path
from unittest import skip
from unittest.mock import patch

from django.urls import reverse
from django_webtest import WebTest
from eve_auth.tools.test_tools import create_fake_user

from catalog.models import App, Review, ReviewReply
from catalog.views import SpecialCategory

from .testdata.factories import AppFactory, CategoryFactory
from .testdata.load_pypi import pypi_info


@patch("catalog.core.pypi.project_info", lambda x: pypi_info)
class TestApps(WebTest):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        cls.category = CategoryFactory(is_default=True)
        cls.user_1001 = create_fake_user(1001, "Bruce Wayne")
        cls.user_1002 = create_fake_user(
            1002, "Peter Parker", permissions=["catalog.can_confirm_ownership"]
        )

    def test_should_create_new_app(self):
        # given
        pypi_name = "app-1"
        self.app.set_user(self.user_1001)
        res = self.app.get(reverse("app-list", args=[SpecialCategory.ALL_APPS]))
        # when
        res = res.click(linkid="btnCreateApp")
        form = res.form
        form["name"] = "My App"
        form["pypi_name"] = pypi_name
        form["confirmation"] = True
        res = form.submit()
        # then
        self.assertEqual(res.status_code, 302)
        self.assertEqual(res.url, reverse("app-detail", kwargs={"slug": pypi_name}))
        self.assertTrue(App.objects.filter(pypi_name=pypi_name).exists())

    def test_should_update_app_name(self):
        # given
        pypi_name = "app-1"
        my_app = AppFactory(name="App 1", pypi_name=pypi_name, category=self.category)
        my_app.maintainers.add(self.user_1001)
        self.app.set_user(self.user_1001)
        res = self.app.get(reverse("app-detail", args=[pypi_name]))
        # when
        res = res.click(linkid="btnEditApp")
        form = res.form
        form["name"] = "App 2"
        res = form.submit()
        # then
        self.assertEqual(res.status_code, 302)
        self.assertEqual(res.url, reverse("app-detail", kwargs={"slug": pypi_name}))
        my_app.refresh_from_db()
        self.assertEqual(my_app.name, "App 2")

    def test_should_add_logo_to_app(self):
        # given
        pypi_name = "app-1"
        my_app = AppFactory(name="App 1", pypi_name="app-1", category=self.category)
        my_app.maintainers.add(self.user_1001)
        self.app.set_user(self.user_1001)
        res = self.app.get(reverse("app-detail", args=[pypi_name]))
        # when
        res = res.click(linkid="btnEditApp")
        form = res.form
        file_path = Path(__file__).parent / "testdata" / "truck.png"
        res = form.submit(upload_files=[("icon", str(file_path))])
        # then
        self.assertEqual(res.status_code, 302)
        self.assertEqual(res.url, reverse("app-detail", kwargs={"slug": pypi_name}))
        my_app.refresh_from_db()
        self.assertRegex(my_app.icon.name, r"uploads/truck.*.png")

    def test_should_not_add_logo_that_is_too_large(self):
        # given
        pypi_name = "app-1"
        my_app = AppFactory(name="App 1", pypi_name="app-1", category=self.category)
        my_app.maintainers.add(self.user_1001)
        self.app.set_user(self.user_1001)
        res = self.app.get(reverse("app-detail", args=[pypi_name]))
        # when
        res = res.click(linkid="btnEditApp")
        form = res.form
        file_path = Path(__file__).parent / "testdata" / "large_logo.png"
        res = form.submit(upload_files=[("icon", str(file_path))])
        # then
        self.assertEqual(res.status_code, 200)

    def test_should_delete_app(self):
        # given
        my_app = AppFactory(
            name="App 1", pypi_name="app-1", category=self.category, is_verified=True
        )
        my_app.maintainers.add(self.user_1001)
        pypi_name = "app-1"
        self.app.set_user(self.user_1001)
        res = self.app.get(reverse("app-detail", args=[pypi_name]))
        # when
        res = res.click(linkid="btnDeleteApp")
        form = res.form
        res = form.submit()
        # then
        self.assertEqual(res.status_code, 302)
        self.assertEqual(res.url, reverse("app-list"))
        self.assertFalse(App.objects.filter(pypi_name=pypi_name).exists())

    def test_should_confirm_app(self):
        # given
        my_app = AppFactory(
            name="App 1", pypi_name="app-1", category=self.category, is_verified=False
        )
        my_app.maintainers.add(self.user_1001)
        pypi_name = "app-1"
        self.app.set_user(self.user_1002)
        res = self.app.get(reverse("app-detail", args=[pypi_name]))
        # when
        res = res.click(linkid="btnConfirmApp")
        # then
        self.assertEqual(res.status_code, 302)
        self.assertEqual(res.url, reverse("app-detail", kwargs={"slug": pypi_name}))
        my_app.refresh_from_db()
        self.assertTrue(my_app.is_verified)

    # def test_should_not_see_confirm_button_without_permission(self):
    #     # given
    #     my_app = AppFactory(
    #         name="App 1",
    #         pypi_name="app-1",
    #         category=self.category_1,
    #
    #         summary="alpha",
    #         description="omega",
    #         is_verified=False,
    #     )
    #     my_app.maintainers.add(self.user_1001)
    #     pypi_name = "app-1"
    #     self.app.set_user(self.user_1001)
    #     # when
    #     res = self.app.get(reverse("app-detail", args=[pypi_name]))
    #     # then
    #     res = res.click(linkid="btnConfirmApp")
    #     self.assertEqual(res.status_code, 302)
    #     self.assertEqual(res.url, reverse("app-detail", kwargs={"slug": pypi_name}))
    #     my_app.refresh_from_db()
    #     self.assertFalse(my_app.is_verified)

    def test_should_not_delete_app(self):
        # given
        my_app = AppFactory(name="App 1", pypi_name="app-1", category=self.category)
        my_app.maintainers.add(self.user_1001)
        pypi_name = "app-1"
        self.app.set_user(self.user_1001)
        res = self.app.get(reverse("app-detail", args=[pypi_name]))
        # when
        res = res.click(linkid="btnDeleteApp")
        res = res.click(linkid="btnCancel")
        # then
        self.assertEqual(res.status_code, 200)
        res.mustcontain("App 1")
        self.assertTrue(App.objects.filter(pypi_name=pypi_name).exists())

    def test_should_show_app_detail_for_normal_app(self):
        # when
        my_app = AppFactory(
            name="App 1",
            pypi_name="app-1",
            category=self.category,
            is_verified=True,
        )
        my_app.maintainers.add(self.user_1001)
        res = self.app.get(reverse("app-detail", args=["app-1"]))
        # then
        self.assertEqual(res.status_code, 200)
        res.mustcontain("App 1")

    def test_should_show_detail_of_unverified_app_to_maintainer(self):
        # given
        self.app.set_user(self.user_1001)
        # when
        my_app = AppFactory(
            name="App 1",
            pypi_name="app-1",
            category=self.category,
            is_verified=False,
        )
        my_app.maintainers.add(self.user_1001)
        res = self.app.get(reverse("app-detail", args=["app-1"]))
        # then
        self.assertEqual(res.status_code, 200)
        res.mustcontain("App 1")

    def test_should_show_detail_of_unverified_app_to_verifier(self):
        # given
        self.app.set_user(self.user_1002)
        # when
        my_app = AppFactory(
            name="App 1",
            pypi_name="app-1",
            category=self.category,
            is_verified=False,
        )
        my_app.maintainers.add(self.user_1001)
        res = self.app.get(reverse("app-detail", args=["app-1"]))
        # then
        self.assertEqual(res.status_code, 200)
        res.mustcontain("App 1")

    def test_should_not_show_detail_of_unverified_app_to_regular(self):
        # when
        my_app = AppFactory(
            name="App 1",
            pypi_name="app-1",
            category=self.category,
            is_verified=False,
        )
        my_app.maintainers.add(self.user_1001)
        res = self.app.get(reverse("app-detail", args=["app-1"]), expect_errors=True)
        # then
        self.assertEqual(res.status_code, 404)


class TestReviews(WebTest):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        category = CategoryFactory(verbose_name="Category 1")
        cls.user_1001 = create_fake_user(1001, "Bruce Wayne")
        cls.app_slug = "app-1"
        cls.my_app = AppFactory(
            name="App 1",
            pypi_name=cls.app_slug,
            category=category,
            is_verified=True,
        )
        cls.my_app.maintainers.add(cls.user_1001)
        cls.user_1002 = create_fake_user(1002, "Peter Parker")

    # FIXME: Test not working
    @skip("Temporary until test is fixed")
    def test_should_create_new_review(self):
        # given
        self.app.set_user(self.user_1002)
        res = self.app.get(reverse("app-detail", args=[self.app_slug]))
        # when
        res = res.click(linkid="btnAddReview")
        form = res.form
        form["summary"] = "Great app"
        form["rating"] = 3
        res = form.submit()
        # then
        self.assertEqual(res.status_code, 302)
        self.assertEqual(res.url, "/apps/detail/app-1?tab=reviews")
        review = App.objects.get(pypi_name=self.app_slug).reviews.first()
        self.assertEqual(review.summary, "Great app")
        self.assertEqual(review.rating, 3)
        self.assertEqual(review.author, self.user_1002)

    def test_should_delete_review(self):
        # given
        review = Review.objects.create(
            app=self.my_app, author=self.user_1002, summary="summary", rating=3
        )
        self.app.set_user(self.user_1002)
        res = self.app.get(reverse("app-detail", args=[self.app_slug]))
        # when
        res = res.click(linkid=f"btnDeleteReview_{review.pk}")
        form = res.form
        res = form.submit()
        # then
        self.assertEqual(res.status_code, 302)
        self.assertEqual(res.url, "/apps/detail/app-1?tab=reviews")
        self.assertFalse(Review.objects.filter(pk=review.pk).exists())

    def test_should_not_delete_review(self):
        # given
        review = Review.objects.create(
            app=self.my_app, author=self.user_1002, summary="summary", rating=3
        )
        self.app.set_user(self.user_1002)
        res = self.app.get(reverse("app-detail", args=[self.app_slug]))
        # when
        res = res.click(linkid=f"btnDeleteReview_{review.pk}")
        res = res.click(linkid="btnCancel")
        # then
        self.assertEqual(res.status_code, 200)
        res.mustcontain("App 1")
        self.assertTrue(Review.objects.filter(pk=review.pk).exists())


class TestReviewReplies(WebTest):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()
        category = CategoryFactory(verbose_name="Category 1")
        cls.user_1001 = create_fake_user(1001, "Bruce Wayne")
        cls.app_slug = "app-1"
        cls.my_app = AppFactory(
            name="App 1",
            pypi_name=cls.app_slug,
            category=category,
            summary="alpha",
            description="omega",
            is_verified=True,
        )
        cls.my_app.maintainers.add(cls.user_1001)
        cls.user_1002 = create_fake_user(1002, "Peter Parker")
        cls.review = Review.objects.create(
            app=cls.my_app, summary="Great App", rating=3, author=cls.user_1002
        )

    def test_should_create_reply(self):
        # given
        self.app.set_user(self.user_1001)
        res = self.app.get(reverse("app-detail", args=[self.app_slug]))
        # when
        res = res.click(linkid=f"btnCreateReply_{self.review.pk}")
        form = res.form
        form["details"] = "Thanks for the review"
        res = form.submit()
        # then
        self.assertEqual(res.status_code, 302)
        self.assertEqual(res.url, "/apps/detail/app-1?tab=reviews")
        reply = self.review.reply
        self.assertEqual(reply.details, "Thanks for the review")
        self.assertEqual(reply.author, self.user_1001)

    def test_should_delete_reply(self):
        # given
        ReviewReply.objects.create(
            review=self.review, author=self.user_1001, details="xyz"
        )
        self.app.set_user(self.user_1001)
        res = self.app.get(reverse("app-detail", args=[self.app_slug]))
        # when
        res = res.click(linkid=f"btnDeleteReply_{self.review.pk}")
        form = res.form
        res = form.submit()
        # then
        self.assertEqual(res.status_code, 302)
        self.assertEqual(res.url, "/apps/detail/app-1?tab=reviews")
        self.review.refresh_from_db()
        with self.assertRaises(Review.reply.RelatedObjectDoesNotExist):
            self.review.reply

    def test_should_not_delete_review(self):
        # given
        self.app.set_user(self.user_1002)
        res = self.app.get(reverse("app-detail", args=[self.app_slug]))
        # when
        res = res.click(linkid=f"btnDeleteReview_{self.review.pk}")
        res = res.click(linkid="btnCancel")
        # then
        self.assertEqual(res.status_code, 200)
        res.mustcontain("App 1")
        self.assertTrue(Review.objects.filter(pk=self.review.pk).exists())
