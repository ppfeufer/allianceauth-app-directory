from django.test import TestCase


class TestContextProcessors(TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        super().setUpClass()

    def test_should_return_common_context(self):
        # when
        response = self.client.get("/")
        # then
        self.assertIn("settings", response.context)
