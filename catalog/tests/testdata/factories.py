import datetime as dt
import random
from typing import Generic, TypeVar

import factory
import factory.fuzzy

from django.utils.text import slugify

from catalog.core.pypi import PyPiProjectAuthor, PypiProjectInfo
from catalog.models import App, Author, Category, Classifier

T = TypeVar("T")

faker = factory.faker.faker.Faker()


def random_classifier_name() -> str:
    words = faker.words()
    result = " :: ".join(words)
    return result


class BaseMetaFactory(Generic[T], factory.base.FactoryMetaClass):
    def __call__(cls, *args, **kwargs) -> T:
        return super().__call__(*args, **kwargs)


class CategoryFactory(
    factory.django.DjangoModelFactory, metaclass=BaseMetaFactory[Category]
):
    class Meta:
        model = Category
        django_get_or_create = ("name",)

    name = factory.Faker("city")
    summary = factory.Faker("sentence")
    verbose_name = factory.LazyAttribute(lambda o: o.name)
    context_style = Category.ContextStyle.INFO


class ClassifierFactory(
    factory.django.DjangoModelFactory, metaclass=BaseMetaFactory[Classifier]
):
    class Meta:
        model = Classifier
        django_get_or_create = ("name",)

    name = factory.LazyFunction(random_classifier_name)


class AuthorFactory(
    factory.django.DjangoModelFactory, metaclass=BaseMetaFactory[Author]
):
    class Meta:
        model = Author
        django_get_or_create = ("name",)

    name = factory.Faker("name")
    email = factory.Faker("email")


class AppFactory(factory.django.DjangoModelFactory, metaclass=BaseMetaFactory[App]):
    class Meta:
        model = App
        django_get_or_create = ("name",)

    category = factory.SubFactory(CategoryFactory)
    is_verified = True
    name = factory.Sequence(lambda n: f"Test App {n + 1}")
    pypi_name = factory.LazyAttribute(lambda o: slugify(o.name))

    description = factory.Faker("paragraph")
    description_content_type = App.ContentType.PLAIN
    first_published = factory.fuzzy.FuzzyDateTime(
        start_dt=dt.datetime(2020, 12, 24, 15, 30, tzinfo=dt.timezone.utc),
        end_dt=dt.datetime(2022, 12, 24, 15, 30, tzinfo=dt.timezone.utc),
    )
    homepage_url = factory.Faker("url")
    last_published = factory.fuzzy.FuzzyDateTime(
        start_dt=dt.datetime(2022, 12, 24, 15, 30, tzinfo=dt.timezone.utc),
        end_dt=dt.datetime(2023, 11, 15, 15, 30, tzinfo=dt.timezone.utc),
    )
    license = "MIT License"
    requirements = ["allianceauth>3"]
    summary = factory.Faker("sentence")

    @factory.lazy_attribute
    def version(obj):
        major = random.randint(0, 5)
        minor = random.randint(0, 20)
        patch = random.randint(0, 20)
        return f"{major}.{minor}.{patch}"

    @factory.post_generation
    def authors(obj, create, extracted, **kwargs):
        if not create or not extracted:
            return

        obj.authors.set(extracted)

    @factory.post_generation
    def maintainers(obj, create, extracted, **kwargs):
        if not create or not extracted:
            return

        obj.maintainers.set(extracted)


class PyPiProjectAuthorFactory(
    factory.Factory, metaclass=BaseMetaFactory[PyPiProjectAuthor]
):
    class Meta:
        model = PyPiProjectAuthor

    name = factory.Faker("name")
    email = factory.Faker("email")


class PypiProjectInfoFactory(
    factory.Factory, metaclass=BaseMetaFactory[PypiProjectInfo]
):
    class Meta:
        model = PypiProjectInfo
        exclude = ("_name",)

    _name = factory.Sequence(lambda n: f"Test App {n + 1}")

    description = factory.Faker("paragraph")
    description_content_type = "text/plain"
    first_published = factory.fuzzy.FuzzyDateTime(
        start_dt=dt.datetime(2020, 12, 24, 15, 30, tzinfo=dt.timezone.utc),
        end_dt=dt.datetime(2022, 12, 24, 15, 30, tzinfo=dt.timezone.utc),
    )
    homepage_url = factory.Faker("url")
    last_published = factory.fuzzy.FuzzyDateTime(
        start_dt=dt.datetime(2022, 12, 24, 15, 30, tzinfo=dt.timezone.utc),
        end_dt=dt.datetime(2023, 11, 15, 15, 30, tzinfo=dt.timezone.utc),
    )
    license = "MIT License"
    name = factory.LazyAttribute(lambda o: slugify(o._name))
    requirements = ["allianceauth>3"]
    summary = factory.Faker("sentence")

    @factory.lazy_attribute
    def authors(obj):
        return [PyPiProjectAuthorFactory()]

    @factory.lazy_attribute
    def classifiers(obj):
        amount = random.randint(0, 10)
        result = [random_classifier_name for _ in range(amount)]
        return result

    @factory.lazy_attribute
    def version(obj):
        major = random.randint(0, 5)
        minor = random.randint(0, 20)
        patch = random.randint(0, 20)
        return f"{major}.{minor}.{patch}"
