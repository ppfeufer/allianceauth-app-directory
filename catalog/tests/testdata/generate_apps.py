# flake8: noqa

import os
import sys
from pathlib import Path

myauth_dir = Path(__file__).parent.parent.parent.parent.parent / "appdir"
sys.path.insert(0, str(myauth_dir))

import django
from django.apps import apps

# init and setup django project
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "appdir.settings.local")
django.setup()

# Main script
from random import randint, shuffle

from django.contrib.auth.models import User
from django.utils.text import slugify
from eve_auth.tools.test_tools import create_fake_user

from catalog.models import App, Category, Review, ReviewReply

LOREM_IPSUM = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

user_1 = User.objects.filter(eve_character__character_id=93135668).first()
if not user_1:
    user_1 = create_fake_user(93135668, "CCP Rise")
user_2 = User.objects.filter(eve_character__character_id=91224790).first()
if not user_2:
    user_2 = create_fake_user(91224790, "CCP Seagull")
user_3 = User.objects.filter(eve_character__character_id=92532650).first()
if not user_3:
    user_3 = create_fake_user(92532650, "CCP Falcon")

verbose_name = "Demo Apps"
category, _ = Category.objects.get_or_create(
    verbose_name=verbose_name, name=slugify(verbose_name)
)

for _ in range(1, 10):
    users = list(User.objects.all())
    shuffle(users)
    id = randint(1000000000, 2000000000)
    name = f"Demo App #{id}"
    pypi_name = slugify(name)
    user = users.pop()
    app, _ = App.objects.get_or_create(
        pypi_name=pypi_name,
        defaults={
            "name": name,
            "category": category,
            "summary": f"Generated entry for {name}",
            "is_verified": True,
            "author": user.username,
            "description": LOREM_IPSUM,
            "description_content_type": App.ContentType.PLAIN,
            "homepage_url": f"https://www.example.com/{pypi_name}",
            "license": "MIT",
            "version": "1.0.0",
        },
    )
    app.maintainers.add(user)
    for user in users[: randint(1, len(users))]:
        review = Review.objects.create(
            app=app,
            summary="Generated review",
            author=user,
            rating=randint(1, 5),
            details=LOREM_IPSUM,
        )
        if randint(1, 3) == 1:
            ReviewReply.objects.create(
                review=review, author=app.maintainers.first(), details=LOREM_IPSUM
            )
