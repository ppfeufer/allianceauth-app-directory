"""Management command to import apps from PyPI."""

import logging

from django.core.management.base import BaseCommand, CommandError

from catalog.models import App, Category

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """A Django management command to import from PyPI."""

    help = "Import apps from PyPI."

    def add_arguments(self, parser):
        default_category = Category.objects.filter(is_default=True).first()
        parser.add_argument(
            "pypi_name",
            nargs="+",
            help="Name of pypi project to import as app. Can be one or multiple.",
        )
        parser.add_argument(
            "--category",
            "-c",
            default=default_category.name if default_category else None,
            help="Name of category to assign imported apps to",
        )

    def handle(self, *args, **options):
        try:
            category = Category.objects.get(name=options["category"])
        except Category.DoesNotExist:
            raise CommandError("No category defined.") from None
        self.stdout.write(
            f"Trying to import {len(options['pypi_name'])} apps from PyPI."
        )
        added_count = 0
        for pypi_name in options["pypi_name"]:
            if App.objects.filter(pypi_name=pypi_name).exists():
                self.stdout.write(
                    self.style.WARNING(
                        f"{pypi_name}: WARNING: This app already exists. Skipped"
                    )
                )
                continue

            app = App(
                category=category, name=pypi_name, pypi_name=pypi_name, is_verified=True
            )
            if app.update_from_pypi():
                added_count += 1
                self.stdout.write(self.style.SUCCESS(f"{pypi_name}: ADDED"))

            else:
                self.stdout.write(
                    self.style.ERROR(
                        f"{pypi_name}: ERROR: This pypi package does not exist"
                    )
                )
        self.stdout.write(f"Imported {added_count} apps from PyPI.")
