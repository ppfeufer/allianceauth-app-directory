"""Fetching information from the Community Creations repo.

https://gitlab.com/allianceauth/community-creations
"""

import logging
from dataclasses import dataclass
from pathlib import PurePosixPath
from typing import List
from urllib.parse import urlparse

import lxml.html
import markdown
import requests

from catalog.app_settings import blacklist

README_URL = (
    "https://gitlab.com/allianceauth/community-creations/-/raw/master/README.md"
)

logger = logging.getLogger(__name__)


@dataclass
class _Repo:
    """A repository mentioned in the README."""

    _HOST_NAMES = {"github.com", "gitlab.com", "bitbucket.org"}

    hostname: str
    path: PurePosixPath
    name: str
    has_pypi: bool = False

    @property
    def pypi_url(self) -> str:
        """Guess the name of the PyPI project"""
        return f"https://pypi.org/pypi/{self.name}/json"

    @classmethod
    def create_from_url_parts(cls, url) -> "_Repo":
        """Create new obj from URL."""
        url_parts = urlparse(url)
        path = PurePosixPath(url_parts.path)
        name = str(PurePosixPath(path.name).with_suffix(""))
        return cls(hostname=url_parts.hostname, path=path, name=name)

    @classmethod
    def create_from_urls(cls, urls) -> List["_Repo"]:
        """Create new obj from URLs."""
        repos = []
        for url in urls:
            url_parts = urlparse(url)
            if url_parts.hostname and url_parts.hostname in cls._HOST_NAMES:
                repos.append(_Repo.create_from_url_parts(url))
        return repos


def repositories() -> List[_Repo]:
    """Return all identified repositories mentioned in the README."""
    response = requests.get(README_URL, timeout=10)
    response.raise_for_status()
    html = markdown.markdown(response.text)
    root = lxml.html.fromstring(html)
    urls = [element.attrib["href"] for element in root.iter() if element.tag == "a"]
    b = blacklist()
    return [o for o in _Repo.create_from_urls(urls) if o.name not in b]
