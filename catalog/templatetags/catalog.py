"""Template tags for catalog."""

from urllib.parse import parse_qs, urlencode, urlparse

from django import template
from django.urls import reverse

register = template.Library()


@register.inclusion_tag("catalog/templatetags/rating_stars.html")
def rating_stars(score):
    """Renders a star field with 5 stars based on given score."""
    if score is None:
        return None
    try:
        score_normalized = max(0, min(5, float(score)))
    except ValueError:
        return None
    score_rounded = round(2 * score_normalized) / 2
    has_half_star = int(score_rounded) < score_rounded
    full_stars_range = range(int(score_rounded))
    empty_stars_range = range(5 - int(score_rounded) - (1 if has_half_star else 0))
    return {
        "score": score_normalized,
        "full_stars_range": full_stars_range,
        "has_half_star": has_half_star,
        "empty_stars_range": empty_stars_range,
    }


@register.simple_tag(takes_context=True)
def nav_active(context, *fragments) -> str:
    """Return 'active' when the string fragment is in the current path.
    When multiple fragments are given they are be concatenated.
    """
    complete_fragment = "".join(fragments)
    try:
        if complete_fragment in context["request"].get_full_path():
            return "active"
    except (KeyError, AttributeError):
        pass
    return ""


@register.simple_tag(takes_context=True)
def nav_active_url(context, url, *args, **kwargs) -> str:
    """Return 'active' when url is the current path."""
    try:
        if reverse(url, args=args, kwargs=kwargs) == context["request"].path:
            return "active"
    except (KeyError, AttributeError):
        pass
    return ""


@register.simple_tag(takes_context=True)
def paging_url(context, page) -> str:
    """Returns url with page query."""
    try:
        page = int(page)
    except (ValueError, TypeError):
        return context["request"].get_full_path()
    result = urlparse(context["request"].get_full_path())
    query = parse_qs(result.query)
    query["page"] = [int(page)]
    new_query = urlencode(query, doseq=True)
    return f"{result.path}?{new_query}" if new_query else result.path
