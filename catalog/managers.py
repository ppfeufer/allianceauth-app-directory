"""Managers for catalog."""

# pylint: disable=missing-class-docstring

import datetime as dt
import logging
from concurrent import futures
from typing import Iterable, Optional

from django.contrib.auth.models import User
from django.db import models
from django.db.models import Avg
from django.utils.timezone import now

logger = logging.getLogger(__name__)

MAX_THREAD_WORKERS = 20


class AppQuerySet(models.QuerySet):
    def user_can_access(self, user):
        """Filter apps that user can access."""
        if user.is_authenticated:
            if user.has_perm("catalog.can_confirm_ownership"):
                apps_qs = self
            else:
                apps_qs = self.filter(is_verified=True) | user.apps.filter(
                    is_verified=False
                )
        else:
            apps_qs = self.filter(is_verified=True)
        return apps_qs

    def annotate_rating_avg(self):
        """Annotate the rating average."""
        return self.annotate(rating_avg=Avg("reviews__rating"))

    def bulk_update_from_pypi(self):
        """Update all apps in bulk from PyPI."""
        logger.info("Started bulk updating %d apps from PyPI...", self.count())
        with futures.ThreadPoolExecutor(max_workers=MAX_THREAD_WORKERS) as executor:
            executor.map(self._thread_update_obj, list(self))
        logger.info("Completed bulk updating %d apps from PyPI.", self.count())

    def _thread_update_obj(self, obj):
        logger.info("Updating app %s from PyPI...", obj.pypi_name)
        obj.update_from_pypi()

    def filter_new_apps(self) -> models.QuerySet:
        """Apply filter for new apps."""
        new_apps_deadline = now() - dt.timedelta(weeks=12)
        return self.filter(created_on__gt=new_apps_deadline)

    def filter_unverified(self) -> models.QuerySet:
        """Apply filter for unverified apps."""
        return self.filter(is_verified=False)

    def filter_user_apps(self, user: User) -> models.QuerySet:
        """Apply filter for apps belonging to a user."""
        return self.filter(id__in=list(user.apps.values_list("id", flat=True)))

    def filter_aa4_compatible(self):
        """Apply filter for AA 4 compatible apps."""
        return self.filter(classifiers__name="Framework :: Django :: 4.2")


class AppManagerBase(models.Manager):
    pass


AppManager = AppManagerBase.from_queryset(AppQuerySet)


class CategoryQuerySet(models.QuerySet):
    def get_default(self) -> Optional[models.Model]:
        """Returns the default category or None."""
        return self.filter(is_default=True).first()


class CategoryManagerBase(models.Manager):
    pass


CategoryManager = CategoryManagerBase.from_queryset(CategoryQuerySet)


class ClassifierManager(models.Manager):
    def get_or_create_many(self, classifiers: Iterable[str]):
        """Get or create many classifiers. Returns list of matching classifier objects."""
        objs = []
        for classifier in classifiers:
            obj, _ = self.get_or_create(name=classifier)
            objs.append(obj)

        return objs
