"""Admin site for catalog."""

# pylint: disable=missing-class-docstring,missing-function-docstring


from typing import Any

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.db.models import Count
from django.db.models.functions import Lower
from django.db.models.query import QuerySet
from django.http.request import HttpRequest
from django.utils.html import format_html

from .models import App, Author, Category, Classifier, Review


class RequiresInline(admin.TabularInline):
    model = App.requires.through
    fk_name = "from_app"

    def has_add_permission(self, *args, **kwargs) -> bool:
        return False

    def has_change_permission(self, *args, **kwargs) -> bool:
        return False


class ReviewInline(admin.TabularInline):
    model = Review

    def has_add_permission(self, *args, **kwargs) -> bool:
        return False

    def has_change_permission(self, *args, **kwargs) -> bool:
        return False


@admin.register(App)
class AppAdmin(admin.ModelAdmin):
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "name",
                    "pypi_name",
                    "authors",
                    "category",
                    "_requirements",
                    "maintainers",
                    "is_verified",
                    "created_on",
                )
            },
        ),
    )
    readonly_fields = ("created_on", "_requirements")
    inlines = [ReviewInline, RequiresInline]
    ordering = ("name",)
    list_display = (
        "name",
        "category",
        "_authors",
        "_maintainers",
        "is_verified",
        "created_on",
    )
    list_filter = (
        ("category", admin.RelatedOnlyFieldListFilter),
        ("authors", admin.RelatedOnlyFieldListFilter),
        ("maintainers", admin.RelatedOnlyFieldListFilter),
        "is_verified",
    )
    search_fields = ("name",)
    filter_horizontal = ("authors", "maintainers")
    actions = ["update_from_pypi", "bulk_confirm_apps"]

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "authors":
            kwargs["queryset"] = Author.objects.order_by(Lower("name"))

        if db_field.name == "maintainers":
            kwargs["queryset"] = get_user_model().objects.order_by(Lower("username"))

        return super().formfield_for_manytomany(db_field, request, **kwargs)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.prefetch_related("authors", "maintainers").select_related()

    @admin.display
    def _authors(self, obj):
        names = [author.name for author in obj.authors.all()]
        return sorted(names)

    @admin.display
    def _maintainers(self, obj):
        users = [user.username for user in obj.maintainers.all()]
        return sorted(users)

    @admin.display
    def _requirements(self, obj):
        return (
            format_html("<br>".join(obj.requirements)) if obj.requirements else "(None)"
        )

    @admin.action(description="Update selected apps from PyPI")
    def update_from_pypi(self, request, queryset):
        queryset.bulk_update_from_pypi()
        self.message_user(request, f"Updated {queryset.count()} apps from PyPI.")

    @admin.action(description="Confirm selected apps")
    def bulk_confirm_apps(self, request, queryset):
        queryset.update(is_verified=True)
        self.message_user(request, f"Confirmed {queryset.count()} apps.")


class AuthorAppInline(admin.TabularInline):
    model = App.authors.through
    fk_name = "author"

    def has_add_permission(self, *args, **kwargs) -> bool:
        return False

    def has_change_permission(self, *args, **kwargs) -> bool:
        return False


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    list_display = ["name", "email", "_app_count"]
    ordering = [Lower("name")]
    inlines = [AuthorAppInline]

    def get_queryset(self, request: HttpRequest) -> QuerySet[Any]:
        qs = super().get_queryset(request)
        return qs.annotate(app_count=Count("apps"))

    @admin.display(ordering="app_count")
    def _app_count(self, obj: App):
        return obj.app_count


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ("verbose_name", "_app_count", "is_default", "context_style")
    readonly_fields = ("name",)
    ordering = ("verbose_name",)
    fields = ("verbose_name", "summary", "is_default", "name", "context_style")

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.annotate(app_count=Count("apps__id"))

    @admin.display(ordering="app_count")
    def _app_count(self, obj):
        return obj.app_count


@admin.register(Classifier)
class ClassifierAdmin(admin.ModelAdmin):
    ordering = ("name",)
    list_display_links = None

    def has_add_permission(self, *args, **kwargs) -> bool:
        return False

    def has_change_permission(self, *args, **kwargs) -> bool:
        return False
