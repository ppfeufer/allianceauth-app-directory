"""Routes for catalog."""

from django.urls import path

from .views import (
    AppConfirmVerificationView,
    AppCreateView,
    AppDeleteView,
    AppDetailView,
    AppListView,
    AppUpdateView,
    DependenciesView,
    FaqView,
    IndexView,
    ReviewCreateView,
    ReviewDeleteView,
    ReviewReplyCreateView,
    ReviewReviewDeleteView,
    export_data,
)

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("dependencies", DependenciesView.as_view(), name="dependencies"),
    path("faq", FaqView.as_view(), name="faq"),
    path("export_data", export_data, name="export-data"),
    path("apps/list", AppListView.as_view(), name="app-list"),
    path("apps/list/<str:category>", AppListView.as_view(), name="app-list"),
    path("apps/detail/<slug:slug>", AppDetailView.as_view(), name="app-detail"),
    path("apps/add", AppCreateView.as_view(), name="app-create"),
    path("apps/update/<slug:slug>", AppUpdateView.as_view(), name="app-update"),
    path(
        "apps/confirm/<int:pk>",
        AppConfirmVerificationView.as_view(),
        name="app-confirm",
    ),
    path("apps/delete/<slug:slug>", AppDeleteView.as_view(), name="app-delete"),
    path(
        "apps/detail/<slug:slug>/add_review",
        ReviewCreateView.as_view(),
        name="review-create",
    ),
    path("reviews/<int:pk>/delete", ReviewDeleteView.as_view(), name="review-delete"),
    path(
        "reviews/<int:pk>/create_reply",
        ReviewReplyCreateView.as_view(),
        name="review-reply-create",
    ),
    path(
        "replies/<int:pk>/delete",
        ReviewReviewDeleteView.as_view(),
        name="review-reply-delete",
    ),
]
