# flake8: noqa
from .base import *

SECRET_KEY = "this is the test environment for tox"


ROOT_URLCONF = "appdir.urls.tox"

# ESI
ESI_SSO_CLIENT_ID = "dummy"
ESI_SSO_CLIENT_SECRET = "dummy"
ESI_SSO_CALLBACK_URL = "http://127.0.0.1:8000/sso/callback"
