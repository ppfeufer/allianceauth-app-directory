# flake8: noqa
from .base import *

SECRET_KEY = "set-your-key-here"

# ALLOWED_HOSTS = ["my-url", "localhost", "127.0.0.1"]

# STATIC_ROOT = "/var/www/myapp/static"

# DATABASES["default"] = {
#     "ENGINE": "django.db.backends.mysql",
#     "NAME": "allianceauth_appdir",
#     "USER": "allianceauth_appdir",
#     "PASSWORD": "my-password",
#     "HOST": "127.0.0.1",
#     "PORT": "3306",
#     "OPTIONS": {"charset": "utf8mb4"},
# }

# ESI
ESI_SSO_CLIENT_ID = "your-id"
ESI_SSO_CLIENT_SECRET = "your-secret"
ESI_SSO_CALLBACK_URL = "your-callback-url"
